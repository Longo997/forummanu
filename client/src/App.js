import React from "react";
import "./App.css";
import { BrowserRouter as Router, Routes, Route, Navigate, BrowserRouter } from "react-router-dom";
import Events from "./components/eventi/events.js";
import Home from './components/home.js';
import Aulastudio from './components/aulastudio.js';
import Layout from './components/layout.js';
import Competizioni from './components/tornei/competizioni.js';
import Info from './components/eventi/info';
import Dettagli from './components/tornei/dettagli';
import Iscrizione from './components/tornei/iscrizione';
import CreaTorneo from './components/tornei/creazione';
import RimuoviTorneo from './components/tornei/rimozione';
import CreaEvento from './components/eventi/creazione';
import RimuoviEvento from './components/eventi/rimozione';
import Login from './components/login/login';
import SignUp from './components/login/SignUp';
import Account from './components/login/account'
import LoginButton from "./components/LoginButton";
import LogoutButton from "./components/LogoutButton";
import Profile from "./components/login/profile";

//<Route path="Login" element={<Login />} />
//<Route path="account" element={<Account />} /> 
//<Route path="SignUp" element={<SignUp />} />

function App() {
  return (
    <>
      <Router>
        <Routes >
           <Route path="/" element={<Layout />}>
            <Route index element={<Home />} />
          <Route path="eventi" element={<Events />} />
          <Route path="login" element={<LoginButton />} />
          <Route path="logout" element={<LogoutButton />} />
          <Route path="competizioni" element={<Competizioni />} />
          <Route path="aulastudio" element={<Aulastudio />} />
          <Route path="info" element={<Info />} />
          <Route path="dettagli" element={<Dettagli />} />
          <Route path="iscrizione/:id" element={<Iscrizione />} />
          <Route path="creazione-torneo" element={<CreaTorneo />} />
          <Route path="rimozione-torneo" element={<RimuoviTorneo />} />
          <Route path="creazione-evento" element={<CreaEvento />} />
          <Route path="rimozione-evento" element={<RimuoviEvento />} />
          <Route path="account" element={<Profile />} />
          </Route>
            
            
        </Routes >
      </Router>
    </>
  );
}


export default App;
