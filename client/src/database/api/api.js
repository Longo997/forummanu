import axios from "axios";


const baseURL = 'http://localhost:3001/api';

export const getPrenotazioniByDate = async (data) => {
    const response = await axios.get(`${baseURL}/prenotazioni/data`, {
        params: {
            data: `${data.toLocaleDateString('en-CA')}`
        }
    })
    return response
}

export const getPrenotazioniByUser = async (user) => {
    const response = await axios.get(`${baseURL}/prenotazioni/user`, {
        headers : {
            'x-access-token': `${sessionStorage.getItem("SavedToken")}`
        }
    })
    return response
}

export const creaPrenotazione = async (id_aula, id_posto, data, token) => {
    console.log(token)
    const prenotazione = { id_aula: `${id_aula}`, id_posto: `${id_posto}`, data: `${data.toLocaleDateString('en-CA')}` };
    const headers = {
        //'x-access-token': `${sessionStorage.getItem("SavedToken")}`
        Authorization: `Bearer ${token}`,
    };
    return axios.post(`${baseURL}/prenotazioni`, prenotazione, { headers })
}

export const getEventi = async () => {
    const response = await axios.get(`${baseURL}/eventi`)
    return response
}

export const getImage = async (image) => {
    const response = await axios.get(`${baseURL}/images`,{
        params: {
            image: `${image}`
        }
    })
    return response.request.responseURL
}

export const deleteEvento = async (idEvento) => {
    const headers = {
        'x-access-token': `${sessionStorage.getItem("SavedToken")}`
    };
    const data = {
        id: `${idEvento}`
    }
    const response = await axios.delete(`${baseURL}/eventi`,{headers, data})
    return response
}

export const login = async (email, password, setAutenticato) => {
    const login = { email: `${email}`, password: `${password}` }
    await axios.post(`${baseURL}/user/login`, login).then((response) => {
        const token = response.data.token
        sessionStorage.setItem("SavedToken", token);
        sessionStorage.setItem("role", response.data.role);
        sessionStorage.setItem("nome", response.data.firstName)
        sessionStorage.setItem("cognome", response.data.lastName)
        setAutenticato((token != null))
    })
        .catch((error) => {
            console.log("Impossibile effetuare Login")
        })
    window.location.reload()
}

export const logout = () => {
    sessionStorage.clear();
}

export const signUp = async (firstName, lastName, email, password) => {
    const signUp = { firstName: `${firstName}`, lastName: `${lastName}`, email: `${email}`, password: `${password}` }
    return await axios.post(`${baseURL}/user/register`, signUp).then((response) => {
        const token = response.data.token
        sessionStorage.setItem("SavedToken", token);
        sessionStorage.setItem("role", response.data.role)
    })
        .catch((error) => {
            console.log("Impossibile effetuare Login")
        })
}

export const createEvento = async (titolo, descrizione, data, numPosti, image) => {
    const evento = new FormData();
    evento.append("titolo", titolo)
    evento.append("descrizione", descrizione)
    evento.append("data", data)
    evento.append("numPosti", numPosti)
    evento.append("image", image)
    const headers = {
        'x-access-token': `${sessionStorage.getItem("SavedToken")}`
    };
    return axios.post(`${baseURL}/eventi`, evento, { headers })
}

export const getTornei = async () => {
    const response = await axios.get(`${baseURL}/tornei`)
    return response
}

export const createTorneo = async (titolo, descrizione, tipologia, data, numPosti) => {
    const torneo = {titolo: `${titolo}`, descrizione: `${descrizione}`, tipologia: `${tipologia}`, data: `${data}`, numPosti: numPosti }
    const headers = {
        'x-access-token': `${sessionStorage.getItem("SavedToken")}`
    };
    return axios.post(`${baseURL}/tornei`, torneo, { headers })
}

export const deleteTorneo = async (idTorneo) => {
    const headers = {
        'x-access-token': `${sessionStorage.getItem("SavedToken")}`
    };
    const data = {
        id: `${idTorneo}`
    }
    const response = await axios.delete(`${baseURL}/tornei`,{headers, data})
    return response
}

export const getIscrizioniSuTorneo = async (torneo) => {
    const response = await axios.get(`${baseURL}/iscrizioni/torneo`,{
        params: {
            idTorneo: `${torneo}`
        }
    })
    return response
}

export const getIscrizioniSuUtente = async (user) => {
    const response = await axios.get(`${baseURL}/iscrizioni/user`, {
        headers : {
            'x-access-token': `${sessionStorage.getItem("SavedToken")}`
        }
    })
    return response
}

export const createIscrizione = async (torneo, nomeSquadra) => {
    const iscrizione = { id_torneo: `${torneo}`, nomeSquadra: `${nomeSquadra}`};
    const headers = {
        'x-access-token': `${sessionStorage.getItem("SavedToken")}`
    };
    return axios.post(`${baseURL}/iscrizioni`, iscrizione, { headers })
}

export const deleteIscrizione = async (idIscrizione) => {
    const headers = {
        'x-access-token': `${sessionStorage.getItem("SavedToken")}`
    };
    const data = {
        id: `${idIscrizione}`
    }
    const response = await axios.delete(`${baseURL}/iscrizioni`,{headers, data})
    return response
}

export const parseJwt = (token) => {
    if (!token) { return "" }
    const base64Url = token.split('.')[1];
    const base64 = base64Url.replace('-', '+').replace('_', '/');
    return JSON.parse(window.atob(base64));
}


