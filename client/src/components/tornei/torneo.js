import React from 'react'
import { useNavigate } from "react-router-dom";
import Card from 'react-bootstrap/Card'
import Button from 'react-bootstrap/Button'
import './tornei.css'

const Torneo = (props) => {
  
    let navigate = useNavigate();

    const handleDettagli = () => {
        navigate("/dettagli", { state: {
            id: props.id, titolo: props.titolo, descrizione: props.descrizione, data: props.data, numPosti: props.numPosti}
        });
    }

    const handleIscrizione = () => {
        navigate(`/iscrizione/${props.id}`, { state: {
            id: props.id, titolo: props.titolo, descrizione: props.descrizione, data: props.data, numPosti: props.numPosti}
        });
    }

    return (
        <>  
     
    
    <Card style={{borderRadius:"2cm", marginTop:'1cm',backgroundImage: "linear-gradient( to right, #ffffff,#bbbbbb)", display:"block", marginLeft:"auto",marginRIght:"auto",  marginBottom:"1cm"}}>
        <Card.Body>
            <Card.Title style={{textAlign:'center'}} > <h2>{props.titolo}</h2></Card.Title>
            <Card.Subtitle style={{textAlign:'center'}}>Data: {props.data}, Posti disponibili: {props.numPosti}</Card.Subtitle>
            <Button style={{WebkitBorderRadius:'1cm',marginTop:'1cm', display:"block", marginRight:"auto",marginLeft:"auto"}} onClick={handleDettagli}>Dettagli</Button>
            <Button variant="success" style={{WebkitBorderRadius:'1cm', marginTop:"1%", display:"block", marginRight:"auto",marginLeft:"auto"}} onClick={handleIscrizione}>Iscrizione</Button>
        </Card.Body>
    </Card>
    
    </>
  )
}

export default Torneo