import React, {useEffect, useState} from 'react'
import { useParams, useLocation } from 'react-router-dom';
import ListGroup from 'react-bootstrap/ListGroup';
import Card from 'react-bootstrap/Card';
import { getIscrizioniSuTorneo } from '../../database/api/api';

const Dettagli = () =>{
    
  const [iscrizioni, setIscrizione] = useState([]);

  let location = useLocation();

  useEffect(() => {
    getIscrizioniSuTorneo(location.state.id).then(response => setIscrizione(response.data))
  }, []);

  return (
    <div>
        <Card style={{marginRight:'auto',marginLeft:'auto',  backgroundImage: "linear-gradient( to right, #ffffff,#bbbbbb)", textAlign: 'center', padding: '2%', marginTop:'2cm'}}>
          <Card.Body>
            <Card.Title>{location.state.titolo}</Card.Title>
            <Card.Subtitle>{location.state.data} posti: {location.state.numPosti}</Card.Subtitle>
            <Card.Text>{location.state.descrizione}</Card.Text>
          </Card.Body>
        </Card>
        <h1 className="chisiamo">Squadre iscritte al torneo </h1>
      {iscrizioni.map(iscrizione => (
        <ListGroup>
          <ListGroup.Item><div className="tutto" style={{width:'3cm',textAlign:'center',marginTop:'1%'}}>{iscrizione.nomeSquadra}</div></ListGroup.Item>
        </ListGroup>)
      )}
    </div>
  )
}

export default Dettagli