import React, {useEffect, useState} from 'react'
import { useParams, useLocation } from 'react-router-dom';
import { useNavigate } from "react-router-dom";
import Swal from 'sweetalert2';
import ListGroup from 'react-bootstrap/ListGroup'
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import { createIscrizione, getIscrizioniSuTorneo } from '../../database/api/api';


const Iscrizione = () => {
  
  const [iscrizioni, setIscrizioni] = useState([])
  const [nomeSquadra, setNomeSquadra] = useState('')

  let location = useLocation();
  let navigate = useNavigate();

  useEffect(()=>{
    getIscrizioniSuTorneo(location.state.id).then(response => setIscrizioni(response.data));
  }, [])


  const postIscrizione = (e) => {
    const torneo = location.state.id
    e.preventDefault()
    if(nomeSquadra===""){
      Swal.fire(
        'Inserire tutti i campi richiesti!'
      );
    }else{
      Swal.fire({
        title: 'Vuoi confermare?',
        text: "Clicca sotto",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ISCRIVI SQUADRA!'
      }).then((result) => {
        if (result.isConfirmed) {
          createIscrizione(torneo, nomeSquadra)
            .then(() => {
              Swal.fire(
                'Squadra iscritta con successo!'
              );
              navigate('/competizioni');

            }
            )
            .catch(() => {
              Swal.fire(
                'Impossibile completare iscrizione!'
              );
            });
        }
      });
    }
  }


  
return(
  <div>
    <h2 className="chisiamo" style={{marginTop:'1cm'}}>Iscrizione squadra:</h2>
  <Form>
  <div className="formiscrizione">
    <Form.Group>
      <Form.Label >Inserisci Nome Squadra</Form.Label>
      <Form.Control type="text" value={nomeSquadra} onChange={(e) => setNomeSquadra(e.target.value)}/>
    </Form.Group>
    <Button className="login" style ={{WebkitBorderRadius:'1cm',marginTop:'1cm'}} onClick={postIscrizione}>Iscriviti</Button>
      </div>
  </Form>
  <hr/>
  
  <h2 className="chisiamo">Squadre iscritte al torneo </h2>
  
  {iscrizioni.map(iscrizione => (
    <ListGroup>
      <ListGroup.Item><div className="tutto" style={{width:'3cm',textAlign:'center',marginTop:'1cm'}}>{iscrizione.nomeSquadra}</div></ListGroup.Item>
    </ListGroup>)
  )}
  </div>
)

}

export default Iscrizione