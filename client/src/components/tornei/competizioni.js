import React, {useEffect, useState} from 'react'
import Torneo from './torneo';
import { Link } from "react-router-dom";
import { getTornei } from '../../database/api/api';
import { FaTrashAlt } from "react-icons/fa";
import { FcPlus } from "react-icons/fc";
import { parseJwt } from '../../database/api/api';
import './tornei.css'

const Competizioni = () => {
  const [tornei, setTornei] = useState([])



  useEffect(() => {
    getTornei().then(response => setTornei(response.data))
  }, []);

  if(parseJwt(sessionStorage.getItem(`SavedToken`)).role == "Admin"){
    return(
      <div className="competitions-list">
        <hr/>
        
        <div>
        <h1 class="eventi">TORNEI</h1>
        </div>

        <Link to={"/creazione-torneo/"}><FcPlus size={40}/></Link>
        <Link to={"/rimozione-torneo/"}><FaTrashAlt size={40} color="red" /></Link>
        <hr/>
        <div className="row">
          {tornei.map(torneo => (
          <Torneo
          id={torneo._id}
          titolo={torneo.titolo}
          data={torneo.data}
          numPosti={torneo.numPosti}
          tipologia={torneo.tipologia}
          descrizione={torneo.descrizione}
          />
          ) )}
        </div>
      </div>
    )
  }
  else {
    return(
      <div className="competitions-list">
        <div>
        <h1 class="eventi">TORNEI</h1>
        </div>

        
        <div className="row">
          {tornei.map(torneo => (
          <Torneo
          id={torneo._id}
          titolo={torneo.titolo}
          data={torneo.data}
          numPosti={torneo.numPosti}
          tipologia={torneo.tipologia}
          descrizione={torneo.descrizione}
          />
          ) )}
        </div>
      </div>
    )
  }

    
}
export default Competizioni