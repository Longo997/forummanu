import Navbar from "react-bootstrap/Navbar";
import Button from 'react-bootstrap/Button'
import { Outlet, Link, useNavigate } from "react-router-dom";
import {RiLogoutBoxRLine} from 'react-icons/ri'
import {RiAccountCircleFill} from 'react-icons/ri'
import { logout } from "../database/api/api";
import { React, useState } from "react"



const Layout = () => {

  let navigate = useNavigate();
  const [autenticato, setAutenticato] = useState((sessionStorage.getItem('SavedToken')) != null)

  const handleExit = () =>{
    if (!autenticato) {
      alert("Effettuare il login!")
    }else{
      logout();
      alert("Logout effettuato correttamente!")
      window.location.reload(false);
    }
  }

  
      return(
      <>
        <Navbar collapseOnSelect expand="lg" bg="light" variant="dark">
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Link to="/"><Button variant="light">Home</Button></Link>
            <Link to="/eventi"><Button variant="light" >Eventi</Button></Link>
            <Link to="/aulastudio"><Button variant="light">Aula Studio</Button></Link>
            <Link to="/competizioni"><Button variant="light">Tornei</Button></Link>
          </Navbar.Collapse>
          <Button variant="light"><Link to="/account"><RiAccountCircleFill size={25} color="blue"/></Link></Button>
          <Button variant="light" onClick={handleExit}><RiLogoutBoxRLine size={25} color="blue"/></Button>
          
        </Navbar>
        <Outlet />
      </>
      )
};

export default Layout;