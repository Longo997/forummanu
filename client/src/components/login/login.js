import './login.css';
import {React} from "react";
import { login } from "../../database/api/api";
import {Link} from 'react-router-dom'
import { useNavigate } from 'react-router-dom';



function Login(props) {

    const navigate = useNavigate();

    const handleClick = () => {
        navigate("/SignUp");
    }


    document.addEventListener('submit', (e) => {
        login(document.getElementById("email").value, document.getElementById("password").value, props.setAutenticato)
        e.preventDefault(true);
    });
    return (
        <form>
            <div className="tutto">
                <div className="form-group">
        
                <input id="email" type="email" className="form-control" placeholder="Email" />
            </div>
            <div className="form-group">
                <input id="password" type="password" className="form-control" placeholder="Password" />
            </div>
            <div className="form-group">
                <div className="custom-control custom-checkbox">
                </div>
            </div>
            <button type="submit" className="login">Login</button>
            <hr>
            </hr>
            <button type="submit" className="signup" onClick={handleClick}>Registrati</button>
            
            </div>
        </form>
    );
}
export default Login
