import React, { useEffect, useState } from 'react'
import Login from '../login/login';
import { getPrenotazioniByUser, getIscrizioniSuUtente, getTornei, deleteIscrizione } from '../../database/api/api.js'
import Button from 'react-bootstrap/Button';
import Swal from 'sweetalert2'
import './account.css'

const Account = () => {
  
  const [autenticato, setAutenticato] = useState((sessionStorage.getItem('SavedToken')) != null)
  const [prenotazioni, setPrenotazioni] = useState([])
  const [nome, setNome] = useState("")
  const [cognome, setCognome] = useState("")     
  const [iscrizioni, setIscrizioni] = useState([])
  const [tornei, setTornei] = useState([])
	

    useEffect(() => {
        getPrenotazioniByUser().then(response => setPrenotazioni(response.data))
        setNome((sessionStorage.getItem('nome')))
        setCognome((sessionStorage.getItem('cognome')))
        getIscrizioniSuUtente().then(risposta => setIscrizioni(risposta.data))
        getTornei().then(result => setTornei(result.data))
      }, []);

          const rows = prenotazioni.filter(a => {
            var data = new Date(a.data)
            return (data >= new Date(new Date().setDate(new Date().getDate()-1)))
        }).map(prenoto => (
          <>
          <div className="prenotazioni">
          <h4>Codice posto: {prenoto.id_posto}</h4>  
          
          <h4>
          {new Intl.DateTimeFormat("it", {
            year: "numeric",
            month: "long",
            day: "2-digit"
          }).format(new Date(prenoto.data))}
          </h4>
          </div>
          </>
        ))

        const doDelete = async (id) => {
          deleteIscrizione(id)
        }

        function handleButtonClick  (id) {

          Swal.fire({
            title: 'Confermi eliminazione?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Conferma!'
          }).then((result) => {
            if (result.isConfirmed) {
              doDelete(id)
                .then(() => {
                  Swal.fire(
                    'Rimozione effettuata con successo!'
                  );
                  window.location.reload(false);
                }
                )
                .catch(() => {
                  Swal.fire(
                    'Impossibile effettuare la rimozione!'
                  );
                });
            }
          });
        }

  if (!autenticato) {
		return (<Login setAutenticato={setAutenticato} />)

	}else{
    return(
        <div>
            <div class="container mt-4 mb-4 p-3 d-flex justify-content-center">
        <div class="card p-4">
        <div class=" image d-flex flex-column justify-content-center align-items-center"> <button class="btn btn-link"> <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTnMellE9JnSgEJsOUTFH5DDiDvdkcT3mJ-_phvoqAMlRXEb1Yi1iRgOeSN0c64UyjnuIE&usqp=CAU" height="100" width="100" /></button> <span class="name mt-3">{nome} {cognome}</span>
        </div>
        </div>
    </div>

    

            <h3 className="chisiamo">Prenotazioni aula studio effettuate: </h3>
            <div>{rows}</div>
            <h3 className="chisiamo">Iscrizioni ai tornei effettuate: </h3>
            {iscrizioni.map(iscrizione => (
            <>
            <div className="prenotazioni">
            <h5>Squadra Iscritta: {iscrizione.nomeSquadra}</h5>
            <h5>Torneo: {tornei.find(torneo => torneo._id === iscrizione.id_torneo).titolo}</h5>
            <Button variant="danger" onClick={() => handleButtonClick(iscrizione._id)}>Cancella</Button>
            </div>
            </>
            ))}
        </div>
)
}
}

export default Account