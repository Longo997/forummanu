import React from "react";
import { signUp } from "../../database/api/api";
import { useNavigate } from 'react-router-dom';


document.addEventListener('submit', (e) => {
    signUp(document.getElementById("firstName").value, document.getElementById("lastName").value, document.getElementById("email").value, document.getElementById("password").value)
    e.preventDefault(true);
});

function SignUp() {


    const navigate = useNavigate();

    const handleClick = () => {
        navigate("/login");
    }



    return (
        <form>
            <div className="tutto">
            <div className="form-group">
                <input id="firstName" type="text" className="form-control" placeholder="Nome" />
            </div>
            <div className="form-group">
                <input id="lastName" type="text" className="form-control" placeholder="Cognome" />
            </div>
            <div className="form-group">
                <input id="email" type="email" className="form-control" placeholder="Email" />
            </div>
            <div className="form-group">
                <input id="password" type="password" className="form-control" placeholder="Password" />
            </div>
            <button type="submit" className="signup2">Registrati</button>
            <hr></hr>
            <p className="forgot-password text-right" style={{textAlign:"center"}}>
                Già regisrato? <a><button type="submit" className="signup3" onClick={handleClick}>Accedi</button></a>
            
            </p>
            </div>
        </form>
    );
}

export default SignUp