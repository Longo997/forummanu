import React from 'react';
import Card from 'react-bootstrap/Card'
import Button from 'react-bootstrap/Button'
import { useNavigate } from 'react-router-dom';

const CardItem = (props) => {

    const navigate = useNavigate();

    const handleClick = () => {
        navigate("/info", { state: {
            id: props.id, title: props.title, body: props.body, date: props.date, numPosti: props.numPosti, image: props.image}
        });
    }

    return(
        
        <Card style={{borderRadius:"5cm", marginTop:'1cm',backgroundImage: "linear-gradient( to right, #ffffff,#bbbbbb)", display:"block", marginLeft:"auto",marginRight:"auto",marginBottom:"1cm",width:"20cm"}}>
            <Card.Body>
                <Card.Title style={{textAlign:'center'}}><h1>{props.title}</h1></Card.Title>
                <Card.Subtitle className="info" style={{textAlign:'center'}}>{props.date}</Card.Subtitle>
                <Button style={{WebkitBorderRadius:'1cm',marginTop:'1cm', display:"block", marginRight:"auto",marginLeft:"auto"}} variant="primary" onClick={handleClick}>Dettagli</Button>
            </Card.Body>
        </Card>
        
    );
    
}

export default CardItem;



