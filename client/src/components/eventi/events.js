import React, { useEffect, useState } from 'react'
import Card from './card';
import { Link } from "react-router-dom";
import { FaTrashAlt } from "react-icons/fa";
import { FcPlus } from "react-icons/fc";
import Row from 'react-bootstrap/Row'
import { getEventi } from '../../database/api/api.js'
import { parseJwt } from '../../database/api/api.js';

const Events = () => {
  const [posts, setPosts] = useState([])

  useEffect(() => {
    getEventi().then(response => setPosts(response.data))
  }, [])

  if(parseJwt(sessionStorage.getItem(`SavedToken`)).role == "Admin"){
    return(<div>

      <div class="container">
        <div class="text-box">
          <h1 class="eventi">EVENTI</h1>
        </div>
          
      </div>
        
      
            <div>
              
              <Link to={"/creazione-evento/"}><FcPlus size={40} /></Link>
              <Link to={"/rimozione-evento/"}><FaTrashAlt size={40} color="red" /></Link>
              <hr/>
              
            <Row>
            {posts.map(card => (
                <Card 
                  id={card._id}
                  title={card.titolo}
                  body={card.descrizione}
                  date={card.data}
                  numPosti={card.numPosti}
                  image={card.imgPath}
                />
              ))}
            </Row>
          </div>
          </div>)
  }
  else{return (
<div>

<div class="container">
	<div class="text-box">
		<h1 class="eventi">EVENTI</h1>
	</div>
		
</div>
	

    <div className="event-list">
    
    
      <Row>
      {posts.map(card => (
          <Card 
            id={card._id}
            title={card.titolo}
            body={card.descrizione}
            date={card.data}
            numPosti={card.numPosti}
            image={card.imgPath}
          />
        ))}
      </Row>
    </div>
    </div>
  )}

  
}
export default Events