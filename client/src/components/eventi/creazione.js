import React, {useState} from 'react'
import { useNavigate } from "react-router-dom";
import { createEvento } from '../../database/api/api';
import Button from 'react-bootstrap/Button';
import './eventi.css'
import Swal from 'sweetalert2';
import Form from 'react-bootstrap/Form';
import { DatePicker } from '@axel-dev/react-nice-dates'
import { it } from 'date-fns/locale'


const CreaEvento = () => {
  
  const [titolo, setTitolo] = useState('')
  const [data, setData] = useState(new Date())
  const [descrizione, setDescrizione] = useState('')
  const [numPosti, setNumPosti] = useState('')
  const [image, setImage] = useState('')

  let navigate = useNavigate();

  const postEvento = (e) => {
    e.preventDefault()
    if(titolo==="" || descrizione==="" || data==="" || numPosti==="" || image===""){
      alert("Inserisci tutti i campi richiesti");
    }else{
      Swal.fire({
        title: 'Vuoi confermare?',
        text: "Clicca sotto",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'CREA EVENTO!'
      }).then((result) => {
        if (result.isConfirmed) {
          const newData = `${data.getDate()}/${data.getMonth()+1}/${data.getFullYear()}`;
          createEvento(titolo, descrizione, newData, numPosti, image)
            .then(() => {
              Swal.fire(
                'Evento creato con successo!'
              );
              navigate('/eventi');

            }
            )
            .catch(() => {
              Swal.fire(
                'Impossibile effettuare la creazione!'
              );
            });
        }
      });
    }
  }

  
  
return(
  <div>



    <div className="formiscrizione">
  <h2>Inserisci i dati dell'evento</h2>
  
  <Form>
  <Form.Group>
  <Form.Label>Data</Form.Label>
  <DatePicker date={data} onDateChange={setData} format='yyyy-MM-dd'  locale={it}>
					{({ inputProps, focused }) => (
						<input
							className={'input' + (focused ? ' -focused' : '')}
							{...inputProps}
						/>
					)}
				</DatePicker>
    <Form.Label>Nome Evento</Form.Label>
    <Form.Control type="text" value={titolo} onChange={(e) => setTitolo(e.target.value)}/>
  </Form.Group>
  <Form.Group>
    <Form.Label>Descrizione</Form.Label>
    <Form.Control type="text" value={descrizione} onChange={(e) => setDescrizione(e.target.value)}/>
  </Form.Group>
  <Form.Group>
    <Form.Label>Numero di posti</Form.Label>
    <Form.Control type="text" value={numPosti} onChange={(e) => setNumPosti(e.target.value)}/>
  </Form.Group>
  <Form.Group>
    <Form.Label htmlFor="file">Carica locandina</Form.Label>
    <input type="file" id="file" accept=".jpg" onChange={event => {
      const file = event.target.files[0]
      setImage(file)
    }}/>
  </Form.Group>
  <Button variant="primary" onClick={postEvento}>Inserisci</Button>
  </Form>
  </div>
  </div>
)
}

export default CreaEvento