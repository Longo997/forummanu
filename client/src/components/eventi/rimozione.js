import React, {useEffect, useState} from 'react'
import { useNavigate } from "react-router-dom";
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import { deleteEvento, getEventi } from '../../database/api/api';
import Swal from 'sweetalert2'

const RimuoviEvento = () => {
  const [posts, setPosts] = useState([])

  let navigate = useNavigate();

  const doDelete = async (id) => {
    deleteEvento(id)
  }

  useEffect(()=>{
    getEventi().then(response => setPosts(response.data))
  }, [])

  function handleButtonClick (id) {

		Swal.fire({
			title: 'Confermi eliminazione?',
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Conferma!'
		}).then((result) => {
			if (result.isConfirmed) {
				doDelete(id)
					.then(() => {
						Swal.fire(
							'Rimozione effettuata con successo!'
						);
						navigate('/eventi');
					}
					)
					.catch(() => {
						Swal.fire(
							'Impossibile effettuare la rimozione!'
						);
					});
			}
		});
	}
  
  return (
    <div>
      <h2>Seleziona evento da eliminare:</h2>
      {posts.map(post=>(
        <Card>
          <Card.Body>
            <Card.Title>{post.titolo}</Card.Title>
            <Card.Subtitle>{post.data} posti: {post.numPosti}</Card.Subtitle>
            <Card.Text>{post.descrizione}</Card.Text>
            <Button variant="danger" onClick={() => handleButtonClick(post._id)}>Seleziona</Button>
          </Card.Body>
      </Card>
      ))}
    </div>
  )
}

export default RimuoviEvento