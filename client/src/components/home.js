import React from 'react'
import '@axel-dev/react-nice-dates/build/style.css'
import Carousel from 'react-bootstrap/Carousel'
import './Carousel.css'
import aulastudio from '../images/aulastudio.jpg'
import ingresso from '../images/ingresso.jpg'
import evento from '../images/evento.jpg'



function Home() {


  return (
    <div>
      
 <h1 class="hashtag">#Forum dei Giovani</h1>
     
     <Carousel fade>
  <Carousel.Item>
    <img
      className="d-block-w-100"
      src={ingresso}
      alt="First slide"
      
    />
 
  </Carousel.Item>
  <Carousel.Item>
    <img
      className="d-block-w-100"
      src={evento}
      alt="Second slide"
      
    />

    
  </Carousel.Item>
  <Carousel.Item>
    <img
      className="d-block-w-100"
      src={aulastudio}
      alt="Third slide"
    />

    
  </Carousel.Item>
</Carousel>
<div className="chisiamo">
<h1> Chi Siamo</h1>
</div>

<h4>Siamo il Forum dei Giovani di Pompei, attivo da Luglio 2019. Promotore sul territorio di
   una pluralità di iniziative, eventi e manifestazioni aventi come destinatari i giovani.
    Da sempre attento ai loro bisogni, capace di ascoltare la loro voce, di raccogliere le loro idee,
     di credere in un futuro migliore e lottare per i loro sogni, perché ciò che è loro fa parte di noi, del nostro essere giovani attivi.</h4>

        </div>
  )
}
export default Home