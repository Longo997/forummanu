const express = require('express')
const bodyParser = require('body-parser')
const routes = require('./lib/routes')
const cors = require('cors')
require("dotenv").config();

const app = express()
app.disable('x-powered-by')
const server = require("http").createServer(app);

const port = process.env.port || 3001

app.use('/api',cors())
app.use(bodyParser.json())
app.use('/lib/uploads', express.static('./lib/uploads'));
routes.addRoutes(app)


if(require.main === module) { 
  server.listen(port, "0.0.0.0");
  console.log("Sei up sulla porta  " + port);
  } else {
  module.exports = app
  }
