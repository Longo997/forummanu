const mongoose = require('mongoose')

const auleSchema = mongoose.Schema({ 
    id: { type: String, unique: true },
    posti: [String]
    })
    
const AuleStudio = mongoose.model('AuleStudio', auleSchema) 
module.exports = AuleStudio