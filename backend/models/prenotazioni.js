const mongoose = require('mongoose')

const prenotazioniSchema = mongoose.Schema({ 
    id_aula: {type:String, index: true},
    id_posto: {type:String, index:true},
    data: {type:Date, index:true},
    id_user: {type:String, index: true}
    })

prenotazioniSchema.index({ id_aula: 1, id_posto: 1 ,data:1}, { unique: true });
prenotazioniSchema.index({ data:1, id_user: 1}, { unique: true });


const Prenotazioni = mongoose.model('Prenotazioni', prenotazioniSchema) 
module.exports = Prenotazioni