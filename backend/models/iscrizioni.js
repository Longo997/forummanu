const mongoose = require('mongoose')

const iscrizioniSchema = mongoose.Schema({ 
    id_torneo: {type:String, index: true},
    id_user: {type:String, index:true},
    nomeSquadra: {type:String, index:true}
    })

iscrizioniSchema.index({ id_torneo: 1, id_user: 1}, { unique: true });  //singola iscrizione utente per un torneo
iscrizioniSchema.index({ id_torneo:1, nomeSquadra: 1}, { unique: true });

const Iscrizioni = mongoose.model('Iscrizioni', iscrizioniSchema) 
module.exports = Iscrizioni