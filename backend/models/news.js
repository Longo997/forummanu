const mongoose = require('mongoose')

const newsSchema = mongoose.Schema({ 
    titolo: String,
    testo: String,
    dataPubblicazione: String
    })
    
const News = mongoose.model('News', newsSchema) 
module.exports = News