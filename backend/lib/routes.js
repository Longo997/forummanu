//const newsHandlers = require('./handlers/newsHandlers')
const eventiHandlers = require('./handlers/eventiHandlers')
const aulaHandlers = require('./handlers/aulaStudioHandlers');
const userHandlers = require('./handlers/userHandlers')
const torneiHandlers = require('./handlers/torneiHandlers')
const prenotazioniHandlers = require('./handlers/prenotazioniHandlers')
const squadreHandlers = require('./handlers/squadreHandlers')
const adminHandlers = require('./handlers/adminHandlers')
const iscrizioniHandlers = require('./handlers/iscrizioniHandlers')
const imagesHandler = require('./handlers/imagesHandlers')
const { uploadImg } = require('../db')
const auth = require("../middleware/auth");
require('express-group-routes');
const {jwtCheck} = require('../middleware/auth2'); 
const { requiredScopes } = require('express-oauth2-jwt-bearer');
const {checkCreaPrenotazione} = require("../middleware/auth2")
var jwtAuthz = require('express-jwt-authz')

//const {requiresAuth} = require('express-openid-connect')

//const checkCreaPrenotazione = requiredScopes('crea:prenotazione');


exports.addRoutes = (app) => {
    app.group("/api", (router) => {

        //Routes per gestire le  PRENOTAZIONI
        router.group("/prenotazioni", (prenotazioniRouter) => {
            prenotazioniRouter.get("/data", prenotazioniHandlers.getPrenotazioniSuData)
            prenotazioniRouter.get("/user", auth, prenotazioniHandlers.getPrenotazioniSuUser)
            //prenotazioniRouter.post("/", requiresAuth(), jwtCheck, prenotazioniHandlers.createPrenotazione)
            prenotazioniRouter.post("/", jwtCheck, checkCreaPrenotazione, prenotazioniHandlers.createPrenotazione)
        })

        //Routes per gestire gli EVENTI
        router.group("/eventi", (eventiRouter) => {
            eventiRouter.get('/', eventiHandlers.getEventi)
            eventiRouter.post('/', auth, uploadImg, eventiHandlers.createEventi)
            eventiRouter.delete('/', auth, eventiHandlers.deleteEvento)
        })

        //Routes per gestire le AULE
        router.group("/aule", (auleRouter) => {
            auleRouter.get('/', aulaHandlers.getAula)
            auleRouter.post('/', auth, aulaHandlers.createAula)
        })

        router.group("/images", (imagesRouter) => {
            imagesRouter.get('/', imagesHandler.retrieveImage)
        })

        //Routes per gestire gli USER
        router.group("/user", (userRouter) => {
            userRouter.post('/register', userHandlers.register)
            userRouter.post('/login', userHandlers.login)
        })

        //Routes per gestire i TORNEI
        router.group("/tornei", (torneiRouter) => {
            torneiRouter.get('/', torneiHandlers.getTornei)
            torneiRouter.post('/', auth, torneiHandlers.createTornei)
            torneiRouter.delete('/', auth, torneiHandlers.deleteTorneo)
        })

        //Routes per gestire le ISCRIZIONI
        router.group("/iscrizioni", (iscrizioniRouter) => {
            iscrizioniRouter.get("/torneo",iscrizioniHandlers.getIscrizioniSuTorneo)
            iscrizioniRouter.get("/user",auth,iscrizioniHandlers.getIscrizioniSuUtente)
            iscrizioniRouter.post("/", auth, iscrizioniHandlers.createIscrizione)
            iscrizioniRouter.delete('/', auth, iscrizioniHandlers.deleteIscrizione)
        })

        //Routes per gestire le SQUADRE
        router.group("/squadre", (squadreRouter) => {
            squadreRouter.get("/", squadreHandlers.getSquadre)
            squadreRouter.post("/", auth, squadreHandlers.createSquadre)
        })

        //Routes per gestire gli ADMIN
        router.group("/admin", (adminRouter) => {
            adminRouter.get('/', adminHandlers.getAdmin)
            adminRouter.post('/', auth, adminHandlers.createAdmin)
        })
    })


}