const db = require('../../db.js')

exports.createPrenotazione = async (req, res) => {
    const { id_aula, id_posto, data } = req.body
    const newData = new Date(data)
    console.log(req)
    await db.createPrenotazioni(id_aula, id_posto, newData, "test", res)
}

exports.getPrenotazioniSuData = async (req, res) => {
    const data = req.query.data
    const newData = new Date(data)
    const prenotazioni = await db.getPrenotazioniData(newData)
    return res.json(prenotazioni)
}

exports.getPrenotazioniSuUser = async (req, res) => {
    const prenotazioni = await db.getPrenotazioniUser(req.user)
    return res.json(prenotazioni)
}