const db = require('../../db.js')

exports.createNews = async (req, res) => {
    const { titolo, testo, dataPubblicazione } = req.body
    await db.createNews(titolo, testo, dataPubblicazione)
    return res.json({ result: 'success' })
}

exports.getNews = async (req, res) => {
    const news = await db.getNews()
    return res.json(news)
}