const db = require('../../db.js')

exports.createEventi = async (req, res) => {
    const { titolo, descrizione, data, numPosti } = req.body
    const image = req.file.path
    await db.createEventi(titolo, descrizione, data, numPosti, image)
    return res.json({ result: 'success' })
}

exports.getEventi = async (req, res) => {
    const eventi = await db.getEventi()
    return res.json(eventi)
}

exports.deleteEvento = async (req, res) => {
    const { id } = req.body
    await db.deleteEvento(id).then((evento) => {
        if (!evento) {
            return res.status(404).send();
        }
        res.send(evento);
    }).catch((error) => {
        res.status(500).send(error);
    })

}