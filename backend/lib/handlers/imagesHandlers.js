const db = require('../../db.js')

exports.retrieveImage = async (req, res) => {
    const path = req.query.image
    await db.getImg(path, res)
}