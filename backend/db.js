const credentials = require('./.credentials.development.json')
const multer = require('multer');
const mongoose = require('mongoose')
var path = require('path');

//Stringa per connettersi al db
const { connectionString } = credentials.mongo

if (!connectionString) {
    console.error('MongoDB connection string missing!')
    process.exit(1)
}

mongoose.connect(connectionString)
const db = mongoose.connection
db.on('error', err => {
    console.error('MongoDB error: ' + err.message)
    process.exit(1)
})
db.once('open', () => console.log('MongoDB connection established'))

const Eventi = require('./models/eventi.js')
const Aula = require('./models/auleStudio.js')
const Tornei = require('./models/tornei.js')
const Prenotazioni = require('./models/prenotazioni')
const Iscrizioni = require('./models/iscrizioni')
const Admins = require('./models/admins')

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './lib/uploads/eventi');
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname);
    }
});

const uploadImg = multer({ storage: storage }).single('image');


module.exports = {
    uploadImg,
    getImg: async (img, res) => {
        var options = {
            root: path.join(__dirname)
        };
        return res.sendFile(img, options);
    },
    createEventi: async (titolo, descrizione, data, numPosti, image) => {
        const evento = new Eventi({ titolo: `${titolo}`, descrizione: `${descrizione}`, data: `${data}`, numPosti: numPosti, imgPath: `${image}` })
        await evento.save()
    },
    getEventi: async () => {
        return await Eventi.find({});
    },
    deleteEvento: async (idEvento) => {
        return await Eventi.findByIdAndDelete(idEvento)
    },
    createAula: async (id, posti) => {
        const aula = new Aula({ id: `${id}`, posti: posti })
        await aula.save()
    },
    getAula: async () => {
        return await Aula.find({});
    },
    createTornei: async (titolo, descrizione, tipologia, data, numPosti) => {
        const torneo = new Tornei({ titolo: `${titolo}`, descrizione: `${descrizione}`, tipologia: `${tipologia}`, data: `${data}`, numPosti: numPosti })
        await torneo.save()
    },
    getTornei: async () => {
        return await Tornei.find({});
    },
    deleteTorneo: async (idTorneo) => {
        await Iscrizioni.deleteMany({ id_torneo: `${idTorneo}` }) //da cambiare in Iscrizioni 
        return await Tornei.findByIdAndDelete(idTorneo)
    },
    getPrenotazioniData: async (data) => {
        return await Prenotazioni.find({ data: data }).exec();
    },
    getPrenotazioniUser: async (user) => {
        return await Prenotazioni.find({ id_user: user.user_id }).exec();
    },
    createPrenotazioni: async (id_aula, id_posto, data, user, res) => {
        const prenotazione = new Prenotazioni({ id_aula: `${id_aula}`, id_posto: `${id_posto}`, data: data, id_user: `${user.user_id}` })
        try {
            await prenotazione.save().then(() => {
                return res.json({ result: 'success' })
            })
        } catch {
            err => console.log(err)
            return res.status(400).send("Impossibile effettuare la prenotazione")
        }
    },
    createIscrizioni: async (id_torneo, nomeSquadra, user, res) => {
        const iscrizione = new Iscrizioni({ id_torneo: `${id_torneo}`, id_user: `${user.user_id}`, nomeSquadra: `${nomeSquadra}` })
        try {
            await iscrizione.save().then(() => {
                return res.json({ result: 'success' })
            })
        } catch {
            err => console.log(err)
            return res.status(400).send("Impossibile effettuare l'iscrizione")
        }
    },
    getIscrizioniSuTorneo: async (torneo) => {
        return await Iscrizioni.find({ id_torneo: torneo }).exec();
    },
    getIscrizioniSuUtente: async (user) => {
        return await Iscrizioni.find({id_user: user.user_id}).exec();
    },
    deleteIscrizione: async (idIscrizione) => {
        return await Iscrizioni.findByIdAndDelete(idIscrizione)
    },
    createAdmin: async (email) => {
        const admin = new Admins({ email: `${email}` })
        await admin.save()
    },
    getAdmin: async () => {
        return await Admins.find({});
    }
    
}