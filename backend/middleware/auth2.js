//const jwt = require('express-jwt');
const { expressjwt: jwt } = require('express-jwt');
var jwks = require('jwks-rsa');
const jwtAuthz = require('express-jwt-authz');

var jwtCheck = jwt({ 
    secret: jwks.expressJwtSecret({ 
        cache: true, 
        rateLimit: true, 
        jwksRequestsPerMinute: 5, 
        jwksUri: 'https://dev-on3j6w2i.us.auth0.com/.well-known/jwks.json' 
    }), 
    audience: 'http://localhost:3001', 
    issuer: 'https://dev-on3j6w2i.us.auth0.com/', 
    algorithms: ['RS256'] 
}); 

//app.use(jwtCheck);

const checkCreaPrenotazione = jwtAuthz(["crea:prenotazione"], {
    customScopeKey: "permissions"
});


module.exports = {
    jwtCheck,
    checkCreaPrenotazione
};
